# Kmar_anaerobic

This repository contains the code used in the processing and visualization of the study:

*Bypassing sterol requirements enables anaerobic growth of the thermotolerant yeast Kluyveromyces marxianus*

Wijbrand J. C. Dekker, Raúl A. Ortiz-Merino, Astrid Kaljouw, Julius Battjes, Frank Wiering, Christiaan Mooiman, Pilar de la Torre, and Jack T. Pronk*

**Abstract**

Current large-scale ethanol production from renewable carbohydrates predominantly relies on anaerobic fermentation by the mesophilic yeast Saccharomyces cerevisiae. Since this production processes benefits from high temperatures, alternative thermotolerant yeast species could represent cost-effective process improvements. However, known thermotolerant yeasts are unable to grow in the absence of oxygen. The response of the thermotolerant yeast Kluyveromyces marxianus and S. cerevisiae to extreme oxygen limitation were analysed in chemostat cultures subjected to different aeration regimes. Physiological responses to sterol supplementation, -omics analyses, and sterol uptake assays, indicated absence of a functional sterol-uptake mechanism as a key factor behind K. marxianus’ inability to grow anaerobically. Heterologous expression of an squalene-tetrahymanol cyclase enabled oxygen-independent production of the sterol surrogate tetrahymanol in K. marxianus. Strains producing  tetrahymanol were successfully adapted to anaerobic growth on glucose at temperatures of up to 45 °C, demonstrating a strategy for developing thermotolerant yeast strains for industrial anaerobic bioprocesses.

**Under review**

# Contributors

[Raul Ortiz-Merino](https://www.tudelft.nl/staff/raul.ortiz/?no_cache=1&cHash=86590dd6db7c6b033cc412087c6154c7) Delft University of Technology

[Wijb Dekker](https://www.tudelft.nl/staff/w.j.c.dekker/?no_cache=1&cHash=f710a8d7a89599d59c95e1c878fa4bf6), 
Delft University of Technology
